package com.example.hw16;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.PhoneStateListener;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class CalAndSMSActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cal_and_s_m_s);

        Button callState = findViewById(R.id.callState);
        Button sendSMSFirst = findViewById(R.id.sendSMSFirst);
        Button sendSMSSecond = findViewById(R.id.sendSMSSecond);

        callState.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                telephonyManager.listen(new PhoneStateListener(){
                    @Override
                    public void onCallStateChanged(int state, String phoneNumber) {
                        super.onCallStateChanged(state, phoneNumber);
                        if(state==TelephonyManager.CALL_STATE_IDLE){
                            Log.d("Hello", "Idle");
                        } else if (state==TelephonyManager.CALL_STATE_RINGING){
                            Log.d("Hello", "Ringing");
                        }
                        else if  (state==TelephonyManager.CALL_STATE_OFFHOOK){
                            Log.d("Hello", "Offhook");
                        }
                        Log.d("Hello", phoneNumber);
                    }
                }, PhoneStateListener.LISTEN_CALL_STATE);
            }
        });

        sendSMSFirst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("sms: +77001002030");
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setData(uri);
                intent.putExtra(Intent.EXTRA_TEXT, "Hello Bruce");
                startActivity(intent);
            }
        });

        sendSMSSecond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(
                        CalAndSMSActivity.this,
                        new String[]{Manifest.permission.SEND_SMS},
                        100
                );
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(
                "+77001002030",
                null,
                "Hello all",
                null,
                null
        );
    }
}